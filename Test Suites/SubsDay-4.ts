<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SubsDay-4</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>deb87249-f062-46c8-a43e-dfd50784869f</testSuiteGuid>
   <testCaseLink>
      <guid>ed5997f3-5a28-4677-bf15-114f9b788db7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subs Day-4/List User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acf99385-037e-4185-86d5-9790076535b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subs Day-4/Update User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>729fe236-7d30-45cb-9f46-a434a17e0e33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subs Day-4/Delete User</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
