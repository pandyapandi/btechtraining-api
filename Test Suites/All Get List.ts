<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All Get List</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0bbbfe75-546b-4e0b-a915-4b1eade1d29c</testSuiteGuid>
   <testCaseLink>
      <guid>48d717e0-7ddb-4aa9-9bec-0c4e6b3c6aaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Get 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15ca9fa3-3318-45e4-a30e-65024049ca1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Get 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f39b696-52b9-4f76-bfd6-8c9e58f5786d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Get ID</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
