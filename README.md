# BtechTraining-API

## Submission Day-4

**Endpoint:**
- [GET] List User = https://reqres.in/api/users?page=2
- [PATCH] Update User = https://reqres.in/api/users/2
- [DELETE] Delete User = https://reqres.in/api/users/2

**Screenshot**

![UserRegister](SS5.png)
